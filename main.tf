variable "revision" {
  default = 1
}

resource "terraform_data" "replacement" {
  input = var.revision
}

output "replacement" {
  value = terraform_data.replacement
}